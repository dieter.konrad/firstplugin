var exec = require('cordova/exec');

exports.coolMethod = function (arg0, success, error) {
// - exports.coolMethod exportiert die Funktion aus dieser Datei. (aus dem Modul)
// - Das Exportieren der Funktion ermöglicht spätere Importieren in einem anderem Script. (einem anderem Modul)
// - Importieren in der NodeJS Umgebung geschieht mit dem wort `require` und nicht mit `import`. (Dies ist eine NodeJS Umgebung)
// - Obere Export Anweisung ist ein benannter Export (). Andere Schreibweise wäre `module.exports`
// - Die Beziechnungn `module` und `exports` sind keine reservierte Wörter in Javascript. (module.exports ist ein Object aus dem NodeJS Modulsystem)

    exec(success, error, 'firstplugin', 'coolMethod', [arg0]);
    // Argumente <success> <error> und <arg0> werden als Parameter in der `coolMethod` Funktion deklariert.
};
